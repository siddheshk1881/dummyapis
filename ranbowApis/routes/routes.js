var express = require('express')
var router = express.Router()
const banner_data = require('../Data/banner.json')
const userProfiles = require('../Data/userprofile.json')                     //home
const recentActivities = require('../Data/recent_activities.json')          //home
const offersHome = require('../Data/offershome.json')                        //home

const redeemelectronics = require('../Data/redeemelectronics.json')                        //home

const homeoffers = require('../Data/offerhome.json')                        //offerhome

const outletoffer = require('../Data/offerlist.json')                       //offerlist

const offerdetails = require('../Data/offerdetail.json')

const dineOffers = require('../Data/dineoffers.json')                        //

const redeemcategories = require('../Data/redeemcategories.json')

const searchresp = require('../Data/searchresp.json')

const drawer = require('../Data/drawer_list.json')

const profiledata = require('../Data/profiledata.json')

const offlinemerchant = require('../Data/offlinemerchant_data.json')

const offlinemerchantdetails = require('../Data/offlinemerchant_details.json')

const myfav = require('../Data/myfav_categories.json')

const giftdetails = require('../Data/giftdetail.json')

const giftbanner = require('../Data/giftcard/banner.json')
const giftOffer = require('../Data/giftcard/offers.json')
const giftPurchase = require('../Data/giftcard/purchase.json')

const homeresp = require('../Data/homepageresp.json')
const earnresp = require('../Data/earnresp.json')

const productlist = require('../Data/productlistresp.json')
const referrals = require('../Data/referral.json')
const notificationlist = require('../Data/notificationlist.json')
const mygiftcard = require('../Data/mygiftcard.json')
const mywallet = require('../Data/mywallet.json')

//2.Home Page 
router.post('/V1/home', (req, res) => {
    res.send({
        "status": true,
        "code": "OFR200",
        "message": "home offer details",
        "values": homeresp
    })
})

//3. Earn Flow Main Page 
router.post('/V1/earnhome', (req, res) => {
    res.send({
        "status": true,
        "code": "OFR200",
        "message": "home offer details",
        "values": earnresp

    })
})

//4.Offers Main Page 
router.post('/V1/offerhome', (req, res) => {
    res.send({
        "status": true,
        "code": "OFHM200",
        "messgae": "message here",
        "banners": banner_data,
        "values": homeoffers.values

    })
})

//5. Offers List Page:  //Done
router.post('/V1/offerlist', (req, res) => {
    res.send({
        "status": true,
        "code": "OFRL200",
        "outlet_found": 8,
        "outletlist": outletoffer
    })
})

//6. Offers Detail Page  
router.post('/V1/offerdetail', (req, res) => {
    res.send({
        "status": true,
        "code": "OFRD200",
        "outletdetailres": offerdetails
    })
})

//7. Offers Thankyou Page
router.post('/V1/offerthankyou', (req, res) => {
    res.send({
        "status": true,
        "code": "GFT200",
        "redemdata":
        {
            "transactionid": "1234567876",
            "redeemstatus": "Successful",
            "code": "TRS200",
            "datetime": "01 Mar 2020 - 03:34 PM",
            "outletname": "Nha Hang Vua Cua",
            "offertitle": "25% off on Total Bill",
            "totalbillamt": "10,010",
            "discount": " 1500",
            "payamt": "15004",
            "savedamt": "2000"
        }
    })
})


//8,20 GiftCardDine   //Done
router.post('/V1/giftcarddine', (req, res) => {
    res.send({
        "status": true,
        "code": "GFT200",
        "message": "Dine Cards",
        "results": 12,
        "category_name": "Dine Giftcards",
        "offers": dineOffers
    })
})

//9. RedeemCategories       //Done
router.post('/V1/redeemcategories', (req, res) => {
    res.send({
        "status": true,
        "code": "RED200",
        "message": "redeem categories",
        "results": 12,
        "category_name": "Electronics",
        "offers": redeemcategories.offers
    })
})

//10. Search Listing Screen  //Done
router.post('/V1/search', (req, res) => {
    res.send({
        "Status": true,
        "code": "SRC200",
        "Message": "search result",
        "response": searchresp
    })
})


//11. Menu drawer       
router.post('/V1/menu', (req, res) => {
    res.send({
        "Status": true,
        "code": "M200",
        "Message": "menu here",
        "drawerlist": drawer
    })
})

//12. My Profile        
router.post('/V1/userprofile', (req, res) => {
    res.send({
        "Status": true,
        "code": "PRO200",
        "Message": "user profile here",
        "profiledata": profiledata.profiledata
    })
})

//13. Offline Merchant Listing Page
router.post('/V1/offlinemerchantlist', (req, res) => {
    res.send({
        "Status": true,
        "code": "OFML200",
        "Message": "offlinemerchant data",
        "data": offlinemerchant
    })
})

//14.Offline Merchant Details
router.post('/V1/offlinemerchantdetails', (req, res) => {
    res.send({
        "Status": true,
        "code": "OFMD200",
        "offerdetailres": offlinemerchantdetails
    })
})

//15. Add new referral      
router.post('/V1/addreferral', (req, res) => {
    res.send({
        "Status": true,
        "code": "NREF200",
        "message": "Referral added successfully"
    })
})

//16. My Favorites          /
router.post('/V1/myfav', (req, res) => {
    res.send({
        "Status": true,
        "code": "FAV200",
        "message": "success",
        "categoriesList": myfav.categoriesList,
        "categoryWiseSubList": myfav.categoryWiseSubList
    })
})

//17. Gift Detail Screen And Redeem Detail Screen Response
router.post('/V1/giftdetail', (req, res) => {
    res.send({
        "Status": true,
        "code": "GFTD200",
        "message": "success",
        "data": giftdetails
    })
})

//18. Gift Card Categories And Redeem Categories    //done
//19. Redeem Detail Screen
router.post('/V1/giftdetailredeem', (req, res) => {
    res.send({
        "Status": true,
        "code": "GFTR200",
        "message": "success",
        "banners": giftbanner,
        "offers": giftOffer
    })
})

//20 Gift Card Dine         //Done
// router.post('/V1/giftcarddine', (req, res) => {
//     res.send({
//         "Status": true,
//         "code": "GPUR200",
//         "message": "success",
//         "results": 12,
//         "category_name": "Dine Giftcards",
//         "offers": giftPurchase
//     })
// })

router.post('/V1/redeemelectronics', (req, res) => {
    res.send({
        "status": true,
        "code": "Redeem100",
        "message": "message here",
        "results": 10,
        "category_name": "Electronics",
        "offers": redeemelectronics.offers
    })
})

router.post('/V1/productlist', (req, res) => {
    res.send({
        "status": true,
        "message": "message here",
        "code": "OFRL200",
        "productCount": 3,
        "banner": banner_data,
        "productlist": productlist
    })
})

router.post('/V1/myreferrals', (req,res) => {
    res.send({
        "status": true,
        "message": "message here",
        "code": "OFRL200",
        "referralCount": 3,
        "referrals": referrals
    })
})

router.post('/V1/purchasedetails', (req,res) => {
    res.send({
        "status": true,
        "message": "Success",
        "giftdata": {
            "brandId": "1",
            "giftId": "1",
            "giftTransactionId": "5642778983",
            "giftConfirmation": "Success",
            "giftItemName": "Vo roof garden",
            "purchaseDate": "2020-03-18T18:30:00.000Z",
            "giftvalue": 5000,
            "quantity": 3,
            "redeemedValue": 10000,
            "giftVoucherCode": "AKSMFRTPE",
            "expiryDate": "2020-08-18T18:30:00.000Z"
        }
    }
    )
})

router.post('/V1/updateprofile', (req,res) => {
    res.send({
        "status": true,
        "message": "Profile Updated Successfully",
    })
})

router.post('/V1/getnotification', (req,res) => {
    res.send({
        "status": true,
         "unread_notification_counts": 3,
         "list" : notificationlist
    })
})

router.post('/V1/mygiftcard', (req,res) => {
    res.send({
        "status": true,
        "message": "message here",
        "data": mygiftcard
    })
})

router.post('/V1/deleteNotification', (req,res) => {
    res.send({
        status: true,
        message: "All Notification Deleted Successfully"
            } 
        )
})

router.post('/V1/mywallet', (rq,res) => {
    res.send({
        "status": true,
        "message": "Success data",
        "current_balance": "RRP 20,000",
        "current_saving": "đ 40,00,000",
        "transaction_data": mywallet
    })
})

router.post('/V1/offermerchantsearch', (req,res) => {
    res.send({
        "status": true,
        "total": "1",
        "data": [
            {
                "offer_image": "images/Lux Fur.png",
                "offer_name": "Luxury Furniture",
                "offer_type": "Home",
                "offer_rrp": "600",
                "offer_address": "Ho Chi Minh",
                "location": "03"
            }
        ]    
    })
})

router.post('/V1/giftthankyoufeedback', (req,res) => {
    res.send({
        "success": true,
        "message": "Feedback received successfully"
    }
    )
} )
module.exports = router
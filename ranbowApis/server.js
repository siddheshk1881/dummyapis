const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');

const app = express();

const PORT = process.env.PORT || 8235


var routes = require('./routes/routes')

app.use('/rest',routes)

app.use('/images', express.static(path.join(__dirname, 'images')))

app.listen(PORT, () => console.log(`express is listening on port ${PORT}`));
